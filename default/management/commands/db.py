﻿from django.core.management.base import BaseCommand
from django.contrib.contenttypes.models import ContentType


class Command(BaseCommand):
    def handle(self, **options):
        for ct in ContentType.objects.all():
            m = ct.model_class()
            print "Model: %s, Records: %d" % (m.__name__, m._default_manager.count())
