﻿from default.models import Requests


class Store(object):
    def process_request(self, request):
        string = 'Request from %s - method %s' % (request.META['REMOTE_ADDR'], request.META['REQUEST_METHOD'])
        r = Requests(request=string)
        r.save()
        return None
