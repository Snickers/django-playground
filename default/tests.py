from django.test import TestCase
from models import Personal


class BaseTest(TestCase):
    def setUp(self):
        Personal.objects.create(first_name='1', last_name='2', email='mail@google.com', phone='3', dob='1988-05-27', bio='asd', id='1')

    def test_Index(self):
        resp = self.client.get('/')
        self.assertEqual(resp.status_code, 200, 'Index page is inaccessible')
        self.assertTrue('data' in resp.context, '"data" is not present in response context')
        self.assertTrue('settings' in resp.context, '"settings" is not present in response context')
        self.assertTemplateUsed(resp, 'index.html', 'Edit template is not being used')
        self.assertTemplateUsed(resp, 'layout.html', 'Layout template is not being used')

    def test_Edit(self):
        resp = self.client.get('/edit/')
        self.assertEqual(resp.status_code, 200, '/edit/ page with query is not available')
        self.assertTemplateUsed(resp, 'edit.html', 'Edit template is not being used')
        self.assertTemplateUsed(resp, 'layout.html', 'Layout template is not being used')
        self.assertTrue('form' in resp.context, '"form" is not present in response context')

    def test_MakeValid(self):
        resp = self.client.get('/make/1/')
        self.assertEqual(resp.status_code, 200, '/make/ page is not available %s' % resp.status_code)
        self.assertTemplateUsed(resp, 'make.html', 'Edit template is not being used')
        self.assertTemplateUsed(resp, 'layout.html', 'Layout template is not being used')
        self.assertTrue('id' in resp.context, '"id" is not present in response context')

    def test_MakeInvalid(self):
        resp = self.client.get('/make/900/')
        self.assertEqual(resp.status_code, 404, '/make/ did not return 404 on invalid request')
