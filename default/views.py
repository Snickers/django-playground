﻿# -*- coding: utf-8 -*-
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponseRedirect, Http404, HttpResponse
from models import Personal, PersonalForm


def index(request):
    return render_to_response('index.html', {'data': Personal.objects.get(id=1), },
                                context_instance=RequestContext(request))


def edit(request):
    data = Personal.objects.get(id=1)
    if request.method == 'POST':
        form = PersonalForm(request.POST, instance=data)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/')
    else:
        form = PersonalForm(instance=data)

    return render_to_response('edit.html', {'form': form, })


def make(request, id):
    try:
        data = Personal.objects.get(id=id)
    except:
        raise Http404()

    return render_to_response('make.html', {'id':id})

def error(request):
    return HttpResponse('Invalid Request - %s' % request.path)
