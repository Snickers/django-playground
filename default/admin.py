from django.contrib import admin
from default.models import Personal


class PersonalAdmin(admin.ModelAdmin):
    list_display = ('first_name', 'last_name', 'email')
    search_fields = ('first_name', 'last_name')

admin.site.register(Personal, PersonalAdmin)
