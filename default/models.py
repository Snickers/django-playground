from django.db import models
from django.forms import ModelForm

from django.db.models.signals import post_save as save
from django.db.models.signals import post_delete as delete


class Personal(models.Model):
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=50)
    dob = models.DateField()
    bio = models.TextField()
    phone = models.CharField(max_length=12)
    email = models.EmailField()

    def __unicode__(self):
        return u'%s %s' % (self.first_name, self.last_name)


class PersonalForm(ModelForm):
    class Meta:
        model = Personal
        fields = ['email', 'phone', 'bio', 'dob', 'last_name', 'first_name']


class Requests(models.Model):
    request = models.CharField(max_length=100)
    time = models.DateTimeField(auto_now=True)


class Operations(models.Model):
    sender = models.CharField(max_length=100)
    action = models.CharField(max_length=20)
    time = models.DateTimeField(auto_now=True)


def save_listener(sender, **kwargs):
    name = sender=sender.__name__
    if name is not 'Operations':
        query = Operations(sender=name, action='save')
        query.save()


def delete_listener(sender, **kwargs):
    name = sender=sender.__name__
    if name is not 'Operations':
        query = Operations(sender=name, action='delete')
        query.save()

save.connect(save_listener)
delete.connect(delete_listener)
