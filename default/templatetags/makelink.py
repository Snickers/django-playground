from django import template
register = template.Library()


def makeLink(did):
    try:
        did = int(did)
        return "<a href='/admin/default/personal/%d/'>Edit</a>" % did
    except:
        pass

register.simple_tag(makeLink)
